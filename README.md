This repository hosts a list of inflectional lexicons in csv format, in long form. These files are intended for use with the [morphalign](https://gitlab.com/sbeniamine/morphalign) and [paradigma](https://gitlab.com/mguzmann89/paradigma) aligners.


- For data used in Beniamine, Sacha and Guzmán Naranjo, Matías (2021) "[Multiple alignments of inflectional paradigms](https://scholarworks.umass.edu/scil/vol4/iss1/21)" *Proceedings of the Society for Computation in Linguistics*: Vol. 4 , Article 21, please see the [SCiL 2021 branch](https://gitlab.com/sbeniamine/inflectionallexicons/-/tree/SCiL-2021), with dev and eval splits.

All are in the same format:

~~~
lexeme,cell,form,language_ID,POS
forfaire,prs.1sg,f O ʁ f E,French,V
gésir,prs.1sg,ʒ i,French,V
surfaire,prs.1sg,s y ʁ f E,French,V
ahaner,prs.1sg,a a n,French,V
abâtardir,prs.1sg,a b a t a ʁ d i,French,V
abattre,prs.1sg,a b a,French,V
~~~

The columns are:

- lexeme: Any lexeme identifier, be it a number, a lemma, etc. Often this is the orthographic form.
- cell: paradigm form. This is also only an identifier, and will not be decomposed, though using meaningful labels is useful for readability.
- form: space separated sequence of phonemes. Each phoneme should be defined in the phonological segments file.
- language_ID: Identifier for the language
- Any other informative columns can be added, such as a POS, etc.

For each language, we also provide a tentative definition of phonological segments in terms of distinctive features.


# Sources

The details of the sources can be found in the file `sources.bib`.

| Language  | Source of the lexicon               | bib refs | license |
| --------- | ----------------------------------- | -------- | ------- |
| Latin     |   [LatinFlexi](https://github.com/matteo-pellegrini/LatInfLexi)  | PellegriniPassarotti2018 |  [![Creative Commons-Attribution-ShareAlike](https://i.creativecommons.org/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/3.0/) |
| Latvian   | [Wiktionnary](https://en.wiktionary.org/), with manual corrections | | [![Creative Commons-Attribution-ShareAlike](https://i.creativecommons.org/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/3.0/) |
| French    |  [Flexique](http://www.llf.cnrs.fr/fr/flexique-fr.php) | BonamiCaronPlancq2014 | [![Creative Commons Attribution-NonCommercial-ShareAlike](https://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/)|
| Hungarian | [Wiktionnary](https://en.wiktionary.org/), with manual corrections | | [![Creative Commons-Attribution-ShareAlike](https://i.creativecommons.org/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/3.0/) |
| Modern Standard Arabic | [Unimorph](https://unimorph.github.io/), phonemized by Beniamine (2018) | Unimorph, BeniaminePHD | [![Creative Commons-Attribution-ShareAlike](https://i.creativecommons.org/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/3.0/) |
| Zenzontepec and Yaytepec Chatino   | [Oto-Manguean Inflectional Class Database](http://www.oto-manguean.surrey.ac.uk/) | FeistPalancar2015 | |

For French, the distinctive features are from Beniamine (2018) and mostly based on Dell (1973). For Latin, the distinctive features are adapted from a table communicated by the authors of LatinFlexi. For Chatino, Modern Standard Arabic, the distinctive features are adapted from [this universal table provided by Bruce Hayes](http://www.linguistics.ucla.edu/people/hayes/120a/index.htm).